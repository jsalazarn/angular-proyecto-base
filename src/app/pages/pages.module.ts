import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BrowserModule } from '@angular/platform-browser';

import { RouterModule } from '@angular/router';

import { PagesComponent } from './pages.component';
import { HomeComponent } from './home/home.component';

@NgModule({
  declarations: [
    PagesComponent, HomeComponent
  ],
  imports: [
    BrowserModule,
    CommonModule,
    RouterModule,
  ]
})
export class PagesModule { }
